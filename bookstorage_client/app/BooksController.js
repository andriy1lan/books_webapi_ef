    
    restApp.controller("BooksController", function($scope, $http) {
    $scope.books;
    $scope.gID;
    $scope.dID;
    $scope.book;
    $scope.ubook;
    $scope.nbook;
    $scope.text;
    $scope.fbooks;
    $scope.message=[];
    //    GET All request function
    $scope.getAll = function() {
      $scope.message[0]="";
      $http.get("http://localhost:2972/api/books").then(
        function successCallback(response) {
          $scope.books = response.data;
        },
        function errorCallback(response) {
          $scope.message[0]="Unable to perform getAll request";
          //console.log("Unable to perform getAll request");
        }
      );
    };

    //    GET request function
    $scope.get = function() {
        $scope.message[1]="";
        $http.get("http://localhost:2972/api/books/"+$scope.gID+"").then(
          function successCallback(response) {
            $scope.book = response.data;
          },
          function errorCallback(response) {
            $scope.book=null;
            var err=response!=undefined ? response.status : "";
            $scope.message[1]="Unable to perform get request"+" "+err;
            //console.log("Unable to perform get request");
          }
        );
      };

    //    GET request function (Find book by author or title - partial search too)
    $scope.find = function() {
      $scope.message[5]="";
      $http.get("http://localhost:2972/api/books/find/"+$scope.text+"").then(
        function successCallback(response) {
          $scope.fbooks = response.data;
        },
        function errorCallback(response) {
          $scope.fbooks=null;
          var err=response!=undefined ? response.status : "";
          $scope.message[5]="Unable to perform get request"+" "+err;
          //console.log("Unable to perform find request");
        }
      );
    };
  
    //    POST request function
    $scope.post = function() {
      $scope.message[2]="";
      $http.post("http://localhost:2972/api/books", $scope.nbook).then(
        function successCallback(response) {
        $scope.message[2]="Book: " + $scope.nbook.Author+ "-"+ $scope.nbook.Title+ "  added";
          console.log("Successfully added data");
        },
        function errorCallback(response) {
          $scope.message[2]="Unable to perform post request";
          //console.log("Adding of data failed");
        }
      );
    };

    //    PUT request function
    $scope.put = function() {
        $scope.message[3]=""
        $http.put("http://localhost:2972/api/books", $scope.ubook).then(
          function successCallback(response) {
          $scope.message[3]="Updated book: " +$scope.ubook.Author+ "-"+ $scope.ubook.Title+ " done";
            console.log("Successfully updated data");
          },
          function errorCallback(response) {
            $scope.message[3]="Unable to perform put request";
            //console.log("Updating of data failed");
          }
        );
      };

      //    DELETE request function
      $scope.delete = function() {
        $scope.message[4]=""
        $http.delete("http://localhost:2972/api/books/"+$scope.dID+"").then(
          function successCallback(response) {
          $scope.message[4]="Book of ID="  + $scope.dID+ " deleted";
            console.log("Successfully deleted data");
          },
          function errorCallback(response) {
            $scope.message[4]="Unable to perform delete request";
            //console.log("Deleting of data failed");
          }
        );
      };

  });
