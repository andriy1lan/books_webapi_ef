'use strict';

var restApp = angular.module("restApp",['ngRoute']);
var logMod = angular.module("logMod", ['ngRoute','ngCookies']);
var Main=angular.module('Main', [
   'restApp',
   'logMod'
]) 

Main
.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/login', {
            controller: 'LoginController',
            templateUrl: 'login.html'
        })
        .when('/books', {
            controller: 'BooksController',
            templateUrl: 'books.html'
        })
        .when('/index1', {
        //    controller: 'BooksController',
            templateUrl: 'index1.html'
        })
        .otherwise({ redirectTo: '/login' });
        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('');
}])

/*

.run(['$rootScope', '$location', '$cookies', '$http',
    function ($rootScope, $location, $cookies, $http) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookies.get('globals') || {};
    //    if ($rootScope.globals.currentUser) {
    //        $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
    //    }
 
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in
            if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
                $location.path('/login');
            }
        });
    }]);
*/