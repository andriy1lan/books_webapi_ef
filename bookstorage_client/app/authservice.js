'use strict';
angular.module('restApp')
.factory ('AuthService', 
['$http', '$cookies', '$rootScope', '$timeout',
function ($http, $cookies, $rootScope, $timeout) {
var service = {};

service.login = function (username, password, callback) {
  //  $scope.message[2]="";
  var response1=false;
    $http.post("http://localhost:2972/api/users/auth", { username: username, password: password }).then(
      function successCallback(response) {
          //response1=true;
          $rootScope.logon=true;
          callback();
          //callback(response1);
      //$scope.message[2]="Book: " + $scope.nbook.Author+ "-"+ $scope.nbook.Title+ "  added";
      },
      function errorCallback(response) {
          callback();
       // $scope.message[2]="Unable to perform post request";
      }
    );
  };

  service.Logout = function () {
    var authdata = 'Basic ' + btoa("Loggedout:Loggedout");
    $rootScope.globals = {
    currentUser: {
    username: 'Loggedout',
    authdata: authdata
    }
    };
    $http.defaults.headers.common['Authorization'] = authdata;
    $cookies.put('globals', $rootScope.globals);
  }



service.SetCredentials = function (username, password) {
var authdata = 'Basic ' + btoa(username + ':' + password);
$rootScope.globals = {
currentUser: {
username: username,
authdata: authdata
}
};
$http.defaults.headers.common['Authorization'] = authdata;
$cookies.put('globals', $rootScope.globals);
}

service.ClearCredentials = function () {
$rootScope.globals = {};
$cookies.remove('globals');
$http.defaults.headers.common.Authorization = 'Basic ';
};

return service;
}]);