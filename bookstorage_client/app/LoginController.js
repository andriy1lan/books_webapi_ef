'use strict';
angular.module("logMod").
controller('LoginController', ['$scope', '$rootScope','$http','$location', '$window', '$q','AuthService',
function($scope, $rootScope, $http, $location,$window,$q, $authService) {
    $scope.issigningup=false;
    $scope.logon=$rootScope.logon;
    //$rootScope.globals!=undefined?$rootScope.globals.currentUser:false;
    $scope.showAll=true;
    $scope.roles=[];
    $scope.editroles=[];
    $scope.username;
    $scope.password;
    $scope.newuser;
    $scope.message;
    $scope.rmessage;
    $scope.getmessage;
    $scope.findwords;
    $scope.users;
    //=[{"Id":3, "Username":"Andriy11","Name":"Andriy","Roles":"Admin,Guest"}]
    $scope.edituser;
    $scope.isediting;
    //$scope.editmessage;
    $authService.ClearCredentials();
    $scope.login = function() {
      //$authService.login($scope.username, $scope.password, function(response1) {
    $authService.login($scope.username, $scope.password, function() {
    if ($rootScope.logon) {
    $authService.SetCredentials($scope.username, $scope.password);                    
    $location.path('/books');
                  } else {
      $scope.message="error"
          }
              });
    };
   

    //    GETAll request function
    $scope.getAll = function() {
      $scope.getmessage="";
      $http.get("http://localhost:2972/api/users").then(
        function successCallback(response) {
          $scope.users = response.data;
        },
        function errorCallback(response) {
          $scope.getmessage="Unable to show Users";
        }
      );
    };

    //    GET request function 
      $scope.get = function(id) {
        var deferred = $q.defer();
        $http.get("http://localhost:2972/api/users/"+id+"").then(
          function successCallback(response) {
           deferred.resolve(response.data);
          },
          function errorCallback(response) {
            deferred.reject(response.data);
            $scope.getmessage="User not found1";
          }
        );
        return deferred.promise;
      };  
    
    //    GET request function (Find user by name, username - partial search too)
      $scope.find = function() {
        $scope.getmessage="";
        $http.get("http://localhost:2972/api/users/find/"+$scope.findwords+"").then(
          function successCallback(response) {
            //$scope.users = response.data;
          },
          function errorCallback(response) {
            $scope.getmessage="Unable to show Users";
          }
        );
      };

    $scope.getUsers = function() {
      if ($scope.showAll) $scope.getAll();
      else $scope.find();
    }  

    $scope.logout = function() {
      $scope.logon=false;
      $rootScope.logon=false;
      //$authService.SetCredentials('Loggedout','Loggedout');
     // $rootScope.globals = {};
     // $cookies.remove('globals');
     // $http.defaults.headers.common.Authorization = undefined;
    }
    $scope.regform = function() {
      $scope.issigningup=true;
    }

    $scope.register = function() {
      $scope.rmessage="";
      var roles;
      if ($scope.roles[0]) {roles="Admin";
      if ($scope.roles[1]) roles+=",Guest"; }
      else if ($scope.roles[1]) { roles="Guest"; }
      $scope.newuser.Roles=roles;
      console.log($scope.newuser);
      $http.post("http://localhost:2972/api/users/post", $scope.newuser).then(
        function successCallback(response) {
          $scope.rmessage="User: " + $scope.newuser.UserName+ "-"+ $scope.newuser.Name+ "  added";
        },
        function errorCallback(response) {
          $scope.rmessage="Unable to perform post request1";
        }
      );
      $scope.issigningup=false;
    }

    $scope.edit = function(id) {
        $scope.get(id).then(function(response) {
        $scope.edituser=response;
        $scope.editroles[0]=$scope.edituser.Roles.split(',')[0]=="Admin";
        $scope.editroles[1]=($scope.edituser.Roles.split(',')[1]=="Guest")||($scope.edituser.Roles.split(',')[0]=="Guest");
        $scope.isediting=true;
        }
        )
    }

    $scope.update = function(id) {
      var roles;
      if ($scope.editroles[0]) {roles="Admin";
      if ($scope.editroles[1]) roles+=",Guest"; }
      else if ($scope.editroles[1]) { roles="Guest"; }
      $scope.edituser.Roles=roles;

          $http.put("http://localhost:2972/api/users", $scope.edituser).then(
          function successCallback(response) {
          $scope.getAll();
          $scope.getmessage="Updated user: " +$scope.edituser.UserName+ "-"+ $scope.edituser.Name+ " done";
          },
          function errorCallback(response) {
            $scope.getmessage="Unable to edit user";
          }
        );
      $scope.isediting=false;
    }

    $scope.delete = function(id) {
      $scope.getmessage=$scope.edituser;
      $http.delete("http://localhost:2972/api/users/"+id+"").then(
          function successCallback(response) {
          $scope.getAll();
          $scope.getmessage="User of ID="  +id+ " deleted";
          },
          function errorCallback(response) {
            $scope.getmessage="Unable to delete user";
          }
        );
    }

                           }]);