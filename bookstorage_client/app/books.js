var uri='http://localhost:2972/api/books';

function getAll() {
    $.getJSON(uri)
        .done(function (data) {
           //$("#maintab").show();
           $("#maintab").css("display","table");
           $('#mainbody').children().empty();
           $.each(data, function (key, item) {
               $('<tr><th>'+item.Id+'</th><th>'+item.Author+'</th><th>'+item.Title+'</th></tr>').appendTo($('#mainbody'));
             });
        })
        .fail(function (jqXHR, textStatus, err) {
            $('#mgetAll').text('Error: ' + err);
        });
}

function find() {
    var word = $('#fword').val();
    $.getJSON(uri + '/find/' + word)
        .done(function (data) {
           $("#findtab").css("display","table");
           $('#findbody').empty();
           $.each(data, function (key, item) {
               $('<tr><th>'+item.Id+'</th><th>'+item.Author+'</th><th>'+item.Title+'</th></tr>').appendTo($('#findbody'));
             });
        })
        .fail(function (jqXHR, textStatus, err) {
            $("#findtab").css("display","none");
            $('#mfind').text('Error: ' + err);
        });
}

function get() {
    $('#mget').empty();
    var idstring = $('#bookid').val();
    var id=parseInt(idstring);
    $.getJSON(uri + '/' + id)
        .done(function (data) {
            $('#bauthor').text(data.Author);
            $('#btitle').text(data.Title);
        })
        .fail(function (jqXHR, textStatus, err) {
            $('#mget').text('Error: ' + err);
        });
}

 function post() {
     $('#mpost').empty();
     var author=$('#new_author').val();
     var title=$('#new_title').val();
     var book={Author:author, Title:title};
     //done(function (data, textStatus, xhr) to get xhr.status
      $.post(uri,book)
        .done(function(data) {
            $('#mpost').text("New book: "+book.Author+"-"+book.Title+" added");
        })
        .fail(function(jqXHR, textStatus, err) {
            $('#mpost').text(err);
        });

 }

 function put() {
    $('#mput').empty();
    var idstring = $('#put_id').val();
    var id=parseInt(idstring);
    var author=$('#put_author').val();
    var title=$('#put_title').val();
    var book={Id:id,Author:author,Title:title};
    $.ajax({  
        url: uri,  
        type: 'PUT', 
        data: book,  
        success: function (data, textStatus, xhr) {  
            $('#mput').text("Updated book: "+book.Author+"-"+book.Title); 
        },  
        error: function (xhr, textStatus, err) {  
            $('#mput').text('Error: '+err);
        }  
    });
 }

 function del() {
    $('#mdelete').empty();
    var idstring = $('#delete_id').val();
    id=parseInt(idstring);
    $.ajax({  
        url: uri + '/' + id,  
        type: 'DELETE',
        success: function (data, textStatus, xhr) {  
            $('#mdelete').text("Book of id:"+id+" deleted");
        },  
        error: function (xhr, textStatus, err) {  
            $('#mdelete').text('Error: '+err);
        }  
    });
}
