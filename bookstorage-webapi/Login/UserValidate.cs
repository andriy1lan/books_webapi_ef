﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using bookstorage_webapi.Repositories;
using bookstorage_webapi.Models;

namespace bookstorage_webapi.Login
{
    public class UserValidate
    {
        //This method is used to check the user credentials
        public static bool Login(string username, string password)
        {
            UserRepository users = new UserRepository();
            var UserList = users.GetUsers();
            return UserList.Any(user =>
                user.UserName.Equals(username, StringComparison.OrdinalIgnoreCase)
                && user.Password == password);
        }

        //This method is used to return the User Details
        public static User GetUserDetails(string username, string password)
        {
            UserRepository users = new UserRepository();
            return users.GetUsers().FirstOrDefault(user =>
                user.UserName.Equals(username, StringComparison.OrdinalIgnoreCase)
                && user.Password == password);
        }
    }
}