﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace bookstorage_webapi.Models
{
    public class UserContext:DbContext
    {
        public UserContext()
            : base()
        {
        }
        public DbSet<User> Users { get; set; }
    }
}