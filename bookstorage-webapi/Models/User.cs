﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace bookstorage_webapi.Models
{
    public class User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Roles { get; set; }

        public User() { }

        public User(string UserName, string Password, string Roles)
        {
            this.UserName = UserName;
            this.Password = Password;
            this.Roles = Roles;
        }

        public User(int Id, string UserName, string Password, string Roles)
        {
            this.Id = Id;
            this.UserName = UserName;
            this.Password = Password;
            this.Roles = Roles;
        }
    }
}