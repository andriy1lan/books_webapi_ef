﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bookstorage_webapi.Models
{
    public class LoginInfo
    {
        public string UserName { get; set; }
        public string Password { get; set; }

        public LoginInfo() { }

        public LoginInfo(string UserName, string Password)
        {
            this.UserName = UserName;
            this.Password = Password;
        }

    }
}