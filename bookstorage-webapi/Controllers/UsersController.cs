﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Diagnostics;

using AttributeRouting;
using AttributeRouting.Web.Http;

using bookstorage_webapi.Models;
using bookstorage_webapi.Login;
using bookstorage_webapi.Repositories;

namespace bookstorage_webapi.Controllers
{
    public class UsersController : ApiController
    {
        private UserRepository repo = new UserRepository();

        // GET api/Users
        public HttpResponseMessage GetUsers()
        {
            IList<User> users = repo.GetUsers();
            if (users.Count != 0)
            {
                return Request.CreateResponse<IList<User>>(HttpStatusCode.OK, users);
            }
            else { return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No users found"); }
        }

        // GET api/Users/5
        public HttpResponseMessage GetUser(int id)
        {
            User user = repo.GetUser(id);
            if (user != null)
            {
                return Request.CreateResponse<User>(HttpStatusCode.OK, user);
            }
            else { return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No such user found"); }
        }

        // POST api/Users - Authentification
        [HttpPost]
        [HttpRoute("api/users/auth")]
        public HttpResponseMessage AuthUser([FromBody]LoginInfo login)
        {
            bool auth = UserValidate.Login(login.UserName,login.Password);
            if (auth)
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else { return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No such user found"); }
        }

        // POST api/Users
        [HttpPost]
        [HttpRoute("api/users/post")]
        public HttpResponseMessage PostUser([FromBody]User user)
        {   
            repo.AddUser(user);
            Debug.WriteLine("11 "+user+" 1");
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, user);
            response.Headers.Location = new Uri(Url.Link("Default", new { id = user.Id }));
            return response;
        }

        // PUT api/Users/5
        public HttpResponseMessage PutUser(User user)
        {
            User user0 = repo.UpdateUser(user);
            if (user0 != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else { return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No such user found"); }
        }

        // DELETE api/Users/5
        public HttpResponseMessage DeleteUser(int id)
        {
            User user = repo.DeleteUser(id);
            if (user != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else { return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No such user id found"); }
        }
        
    }
}