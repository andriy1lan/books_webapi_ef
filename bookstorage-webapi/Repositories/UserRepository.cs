﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data;

using bookstorage_webapi.Models;

namespace bookstorage_webapi.Repositories
{
    public class UserRepository
    {
        UserContext context = new UserContext();

        public IList<User> GetUsers()
        {
            return context.Users.ToList();
        }

        public IList<User> GetUsers(String text)
        {
            IList<User> users = context.Users.Where(e => e.UserName.Contains(text) || e.Roles.Contains(text)).ToList<User>();
            return users;
        }

        public User GetUser(int id)
        {
            return context.Users.Find(id);
        }

        public User UpdateUser(User item)
        {
            //var user = context.Users.Find(item.Id);
            try
            {
                 context.Entry(item).State = EntityState.Modified;
                //context.Entry(user).CurrentValues.SetValues(item);
            }
            catch (Exception e) { }
            context.SaveChanges();
            return item;
        }

        public User AddUser(User item)
        {
            context.Users.Add(item);
            context.SaveChanges();
            return item;
        }

        public User DeleteUser(int id)
        {
            User user = context.Users.Find(id);
            try
            {
                context.Users.Remove(user);
            }
            catch (Exception e) { }
            context.SaveChanges();
            return user;
        }
    }
}