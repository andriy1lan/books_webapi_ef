﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bookstorage_webapi.Repositories
{
 public interface IRepository<T>
    {
        IList<T> GetBooks();
        IList<T> GetBooks(String s);
        T GetBook(int id);
        T UpdateBook(T item);
        T AddBook(T item);
        T DeleteBook(int id);
    }
}
